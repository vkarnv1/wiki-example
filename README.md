# wiki-example

[Link](https://gitlab.com/himinds-pub/wiki-example/-/wikis/ci-report) to wiki [CI-Report](https://gitlab.com/himinds-pub/wiki-example/-/wikis/ci-report).

# Example of the tabel we create from our pipeline

## Report

### created: 2021-06-22 17:46:38


Results from the last run.

| Name | Ref | Status |
| ---- | --- | ------ |
| angularmessenger | master | success
embedded-env-sensor-g2 | master | success
embedded-env-sensor-mqtt | master | success
backend-mqtt-database-service | master | success |
